# nginx + certbot role

Designed for Debian.

It doesn't make sense to deal with web hosting and certbot separately, so they're in the same role. However if you really need to (e.g. when setting up a local server) you can skip the certbot tasks by setting `skip_certbot` to `true`.  This role will:

1. Install certbot and nginx and set them up
2. Get initial certificates for all specified domains
3. Install specified site configs

See the last section for help on how to **[add a new domain to an existing host](#when-you-add-a-domainmany-domains-to-an-existing-host)**.

## Certbot

Certbot will be set up with auto-renew using systemd (not cron).

Variables you need to set:

```yml
certbot_email: <email address>
certbot_domains:
 - <primary SSL domain>
 - <secondary SSL domains, if any, on the same cert>
```

All domains are on the same certificate to avoid SNI fuckery for browsers running on Windows XP.

To skip the certbot tasks set:
```yml
skip_certbot: true
```

## Nginx

Nginx will be set up with:
 - [Mozilla-recommended](https://wiki.mozilla.org/Security/Server_Side_TLS#Pre-defined_DHE_groups) Diffie-Hellman groups in /etc/nginx/ssl/
 - Default settings more or less in line with HTML5 Boilerplate
 - Mime types from HTML5 Boilerplate

You should provide the site configs that you want in the `nginx_sites` variable.  This is a list of dicts, that can have `name` and `enabled` keys, e.g.

```yml
nginx_sites:
 - name: mainsite
 - name: secondary-domain
   enabled: false
```

The `name` of the site will be used to find the template to install in `/etc/nginx/sites-available`, so for the above example, `templates/mainsite` will be installed there.  `enabled` determines whether the site is also linked into `/etc/nginx/sites-enabled`, which `secondary-domain` wouldn't be.

## Initial certificates

When you first run this role on a fresh server, it will set up a single nginx site config which listens for all the sites listed in `certbot_domains`.  It will then get the certificates, and afterwards unlink that config file.

From then onwards, your individual site config files (specified in `nginx_sites`) need to have the right bit of config for certificate renewal.  It should look something like this:

```nginx
    server {
        listen 80;
        listen [::]:80;
    
        server_name whatever.it.is.xyz;
    
        location /.well-known/acme-challenge {
            root /var/www/letsencrypt;
            try_files $uri $uri/ =404;
        }
    
        location / {
            return 301 https://$host$request_uri;
        }
    }

    server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
    
        server_name whatever.it.is.xyz;
    
        location /.well-known/acme-challenge {
            root /var/www/letsencrypt;
            try_files $uri $uri/ =404;
        }

        # The rest of your config
    }
```

## When you add a domain/many domains to an existing host

1. Add the domain(s) to `certbot_domains`.
2. Set `certbot_add_domains` to a list of the domains you are adding.  This should be a list of one if there's only one domain.
3. Run the playbook, with `--tags=certbot-add,nginx-sites`.
4. Bask in the glory of things just working.

Behind the scenes, this does basically the same thing as when you run the role initially, but just for the added sites:

1. Add an nginx config file that listens on port 80 for the new sites.
2. Get the certificates.
3. Delete that config file.
4. Install your production configs as listed in `nginx_sites`.
